/*

 Copyright (C) 2022-2023 programmerpony

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import fetch from 'node-fetch';
import dateFormat from 'dateformat';
import express from 'express';
import bodyParser from 'body-parser';
import RSS from 'rss';
import 'dotenv/config';
const app = express();

app.enable('trust proxy');
app.set('views', 'src/views');
app.set('view engine', 'ejs');
app.use(express.static('src/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get(['/','/list/:page'], async (req, res) => {
  try {
    let comics = await fetch(`https://yiffer.xyz/api/comicsPaginated?page=${req.params.page || req.query.page || 1}`).then(r => r.json());
    if (comics.comics.length === 0) res.status(404);
    res.render('list.ejs', {comics:comics, query:''});
  } catch (e) {
    res.status(504);
    res.render('error.ejs', {
      error: 'Unable to fetch data from Yiffer.xyz!',
      info: 'Maybe their site is down. Please try again later.'
    });
  }
});

app.get('/list.rss', async (req, res) => {
  try {
    let comics = await fetch('https://yiffer.xyz/api/all-comics').then(r => r.json());
    let feed = getFeed('All comics - Yiffest!', req, comics);
    res.set('Content-Type', 'application/rss+xml');
    res.send(feed);
  } catch (e) {
    res.status(504);
    res.send('Unable to fetch data from Yiffer.xyz!');
  }
});

app.get('/search', async (req, res) => {
  try {
    if (!req.query.tags) return res.redirect('/');
    let tags = await parseQuery(req.query.tags);
    req.query.page ||= 1;
    let comics = tags ? await fetch(`https://yiffer.xyz/api/comicsPaginated?page=${req.query.page}&${tags}`).then(r => r.json()) : {};
    if (comics.comics?.length === 0) res.status(404);
    res.render('list.ejs', {comics:comics, query: req.query.tags});
  } catch (e) {
    res.status(504);
    res.render('error.ejs', {
      error: 'Unable to fetch data from Yiffer.xyz!',
      info: 'Maybe their site is down. Please try again later.'
    });
  }
});

app.get('/query.rss', async (req, res) => {
  try {
    if (!req.query.tags) return res.redirect('/list.rss');
    let tags = await parseQuery(req.query.tags);
    if (!tags) {
      res.status(404);
      return res.send('Invalid tags.');
    }
    let comics = await fetch(`https://yiffer.xyz/api/comicsPaginated?${tags}`).then(r => r.json());
    if (comics.comics.length === 0) res.status(404);
    let feed = getFeed(`Query: ${req.query.tags} - Yiffest!`, req, comics.comics);
    res.set('Content-Type', 'application/rss+xml');
    res.send(feed);
  } catch (e) {
    res.status(504);
    res.send('Unable to fetch data from Yiffer.xyz!');
  }
});

app.get('/:comic', async (req, res) => {
  try {
    let comic = await fetch(`https://yiffer.xyz/api/comics/${req.params.comic}`).then(r => r.json());
    comic.created = dateFormat(comic.created, 'mmm dS yyyy');
    comic.updated = dateFormat(comic.updated, 'mmm dS yyyy');
    res.render('comic.ejs', {comic:comic});
  } catch (e) {
    if (e.toString().startsWith('SyntaxError')) {
      res.status(404);
      res.render('error.ejs', {
        error: 'Comic not found!',
        info: 'Maybe the comic was removed from Yiffer.xyz :('
      });
    } else {
      res.status(504);
      res.render('error.ejs', {
        error: 'Unable to fetch data from Yiffer.xyz!',
        info: 'Maybe their site is down. Please try again later.'
      });
    }
  }
});

app.get('/artist/:artist.rss', async (req, res) => {
  try {
    let artist = await fetch(`https://yiffer.xyz/api/artists/${req.params.artist}`).then(r => r.json());
    let feed = getFeed(`Artist: ${req.params.artist} - Yiffest!`, req, artist.comics);
    res.set('Content-Type', 'application/rss+xml');
    res.send(feed);
  } catch (e) {
    if (e.toString().startsWith('SyntaxError')) {
      res.status(404);
      res.send('Artist not found!');
    } else {
      res.status(504);
      res.send('Unable to fetch data from Yiffer.xyz!');
    }
  }
});

app.get('/artist/:artist', async (req, res) => {
  try {
    let artist = await fetch(`https://yiffer.xyz/api/artists/${req.params.artist}`).then(r => r.json());
    artist.name = req.params.artist;
    res.render('artist.ejs', {artist:artist});
  } catch (e) {
    if (e.toString().startsWith('SyntaxError')) {
      res.status(404);
      res.render('error.ejs', {error:'Artist not found.'});
    } else {
      res.status(504);
      res.render('error.ejs', {
        error: 'Unable to fetch data from Yiffer.xyz!',
        info: 'Maybe their site is down. Please try again later.'
      });
    }
  }
});

app.listen(process.env.PORT || 3000);

const comicTag = ['M','F','MF','MM','FF','MF+','I'];
async function parseQuery(query) {
  // Remove extra commas and spaces so that the query doesn't fail
  while (query.includes(' ,')) query = query.replace(' ,',',');
  while (query.includes(', ')) query = query.replace(', ',',');
  while (query.includes(',,')) query = query.replace(',,',',');
  while (query.includes('  ')) query = query.replace('  ',' ');
  if (query.startsWith(',')) query = query.slice(1);
  if (query.endsWith(',')) query = query.slice(0,-1);
  if (query.startsWith(' ')) query = query.slice(1);
  if (query.endsWith(' ')) query = query.slice(0,-1);
  query = query.split(',');
  // These tags are a separate property of comics
  let tags = query.filter(t=>comicTag.includes(t.toUpperCase()));
  tags = tags.length ? `tags[]=${tags.join('&tags[]=')}` : null;
  // Convert keyword names to IDs
  let keywords = await fetch('https://yiffer.xyz/api/keywords').then(r => r.json());
  keywords = keywords.filter(k => query.includes(k.name)).map(k => k.id).join('&keywordIds[]=');
  if (!keywords && !tags) return;
  return keywords ? `keywordIds[]=${keywords}${tags ? `&${tags}` : ''}` : tags;
}

function getFeed(title, req, comics) {
  let feed = new RSS({title:title,site_url:`${req.protocol}://${req.get('host')}`});
  for (let comic of comics) {
    let content = `A${comic.state=='wip'?' WIP':''} comic by ${comic.artist}<br><br>`;
    for (let i = 1; i < comic.numberOfPages; i++) {
      content = `${content}<img src="https://static.yiffer.xyz/comics/${comic.name}/${i.toString().padStart(3,'0')}.jpg"><br><br>`
    }
    feed.item({
      title: comic.name,
      author: comic.artist,
      date: comic.updated,
      url: `${req.protocol}://${req.get('host')}/${comic.name}`,
      guid: `${comic.name}${comic.updated}`, // The comic will show as unread in the RSS reader if it got an update
      description: content
    });
  }
  return feed.xml({indent:true});
}