# Yiffest!
Yiffest! is an alternative front-end for [yiffer.xyz](https://yiffer.xyz) that runs **no JavaScript** on the client.

# Disclaimer
This project is not affiliated with nor endorsed in any way by Yiffer.xyz.

# Public instances
- [yiffest.programmerpony.com](https://yiffest.programmerpony.com/)
- [yiffest.glitch.me](https://yiffest.glitch.me/)

If you want your instance added to this list, open a pull request on this repo.

# Building
If you want to self-host, do the following:

1. Install Git, Node.js 16.14.2 or higher, and npm.
2. Run the following commands:
    ```bash
    git clone https://codeberg.org/programmerpony/yiffest
    cd yiffest
    npm install
    npm start
    ```
3. Open `http://localhost:3000` on your web browser.
4. ???
5. PROFIT!!

Before hosting an instance on the internet, make sure to read the license.

# License

```
Copyright (C) 2022-2024 programmerpony

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
tl;dr no proprietary instances allowed